package com.ims.repository;

import com.ims.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(nativeQuery = true, value = "select * from Product p order by p.id desc limit 1")
    Product findLastCreatedProduct(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM Product p WHERE lower(CONCAT(p.name, ' ', p.category, ' ', p.price)) LIKE lower(concat('%', trim(?1),'%'))")
    List<Product> searchProductsByKeyword(String keyword);

    @Query(
            nativeQuery = true,
            value = "SELECT p.id " +
                    "FROM Customer c " +
                        "INNER JOIN Product p ON p.customer_id = c.id " +
                    "WHERE c.id = :customerId"
    )
    List<Long> findAllByCustomerId(Long customerId);

}
